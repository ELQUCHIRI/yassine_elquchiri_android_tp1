package com.example.tp1;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;


public class MainActivity extends AppCompatActivity {

    public static AnimalList animalList = new AnimalList();
    public RecyclerView animalRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAnimalRecyclerView();

    }

    private void initAnimalRecyclerView() {

        animalRecyclerView = findViewById(R.id.aaa);

        ArrayList<String> listNames = new ArrayList<String>();
        Collections.addAll(listNames, animalList.getNameArray());

        AnimalRecyclerAdapter adpater = new AnimalRecyclerAdapter(this, listNames);

        animalRecyclerView.setAdapter(adpater);
        animalRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
