package com.example.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {

    private Animal animal;

    private TextView name;
    private TextView esperenceVie;
    private TextView periodeGestation;
    private TextView poidsNaissance;
    private TextView poidsAdulte;

    private EditText statutConversation;

    private ImageView imageAnimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        name = findViewById(R.id.nameAnimal);
        esperenceVie = findViewById(R.id.esperenceVie);
        periodeGestation = findViewById(R.id.periodeGestation);
        poidsNaissance = findViewById(R.id.poidsNaissance);
        poidsAdulte = findViewById(R.id.poidsAdulte);

        statutConversation = findViewById(R.id.statutConversation);

        imageAnimal = findViewById(R.id.imageAnimal);

        Intent intent = getIntent();

        String name = intent.getStringExtra("name");

        setView(name);
    }

    private void setView(String name) {

        this.animal = AnimalList.getAnimal(name);

        this.name.setText(name);
        this.esperenceVie.setText(animal.getStrHightestLifespan());
        this.periodeGestation.setText(animal.getStrGestationPeriod());
        this.poidsNaissance.setText(String.valueOf(animal.getStrBirthWeight()));
        this.poidsAdulte.setText(animal.getStrAdultWeight());
        this.statutConversation.setText(animal.getConservationStatus());
        this.imageAnimal.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName())));

    }

    public void save(View view) {
        this.animal.setConservationStatus(this.statutConversation.getText().toString());
        Toast.makeText(AnimalActivity.this, "Save", Toast.LENGTH_LONG).show();
    }
}
